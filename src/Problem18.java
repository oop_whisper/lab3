import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int star;
        int num;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Please select star type [1-4,5 is Exit]: ");
            star= sc.nextInt();
            if (star == 5) {
                System.out.println("Bye bye!!!");
                break;
            }

            switch (star) {
                case 1:
                System.out.print("Please input number: ");
                num = sc.nextInt();
                for(int i = 0; i< num; i++) {
                    for(int j = 0; j<i+1; j++){
                        System.out.print("*");
                    }
                    System.out.println();
                }    
                    break;
                case 2:
                    System.out.print("Please input number: ");
                    num = sc.nextInt();
                    for(int i = num; i>0; i--) {
                        for(int j = 0; j<i; j++){
                           System.out.print("*");
                         }  
                         System.out.println(); 
                    }
    
                    break;
                case 3:
                    System.out.print("Please input number: ");
                    num = sc.nextInt();
                    for(int i = 0; i<num; i++) {
                        for(int j = 0; j<i; j++) {
                           System.out.print(" ");
                         }  
                         for(int j = 0; j<num-i; j++) {
                           System.out.print("*"); 
                         }
                         System.out.println();
                    }
                    break;
                case 4:
                    System.out.print("Please input number: ");
                    num = sc.nextInt();
                        for(int i=num; i>=0; i--) {
                            for(int j=0; j<num; j++){
                              if(j>=i){
                                   System.out.print("*");
                                } else {
                                    System.out.print(" ");               
                                }  
                            } 
                            System.out.println();    
                        }
                    break;
                default:
                    System.out.println("Error: Please input number between 1-5");
                 
                       break;
            }
        }
        sc.close();
    }
}
